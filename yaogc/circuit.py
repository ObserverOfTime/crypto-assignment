from typing import Tuple

from .gate import InputGate, LogicGate
from .op import OP


def full_adder(a: int, b: int, cin: int) -> Tuple[LogicGate, LogicGate]:
    """
    Create a full adder circuit.

    :param a: The first input bit.
    :param b: The second input bit.
    :param cin: The carry bit.
    :return: The ``carry`` and ``sum`` gates.
    """
    gate_a = InputGate(a)
    gate_b = InputGate(b)
    gate_c = InputGate(cin)
    gate_d = LogicGate(OP.XOR, gate_a, gate_b)
    gate_e = LogicGate(OP.AND, gate_a, gate_b)
    gate_f = LogicGate(OP.AND, gate_c, gate_d)
    gate_g = LogicGate(OP.OR, gate_e, gate_f)
    gate_h = LogicGate(OP.XOR, gate_c, gate_d)
    return gate_g, gate_h
