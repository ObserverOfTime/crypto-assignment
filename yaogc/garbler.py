from itertools import zip_longest
from gzip import compress, decompress
from pickle import dumps, loads
from pickletools import optimize
from typing import Iterator, Tuple

from .circuit import full_adder


class Garbler:
    """
    The person that garbles the circuit.

    :arg a: The first number.
    :arg b: The second number.
    """
    def __init__(self, a: int, b: int):
        a, b = format(a, 'b'), format(b, 'b')
        self.__pairs = zip_longest(a[::-1], b[::-1], fillvalue='0')
        self.__lookups = {}
        self.__offset = 0
        self.__cout = 0
        self.__sum = 0

    def __iter__(self) -> Iterator[bytes]:
        """
        Iterate over the bits of the numbers in reverse
        and returns the ``carry`` and ``sum`` gates.

        :return: Compressed and pickled gate pairs.
        """
        for a, b in self.__pairs:
            cout, sum_ = full_adder(int(a), int(b), self.__cout)
            kg0, kg1, _ = cout.output
            kh0, kh1, _ = sum_.output
            self.__lookups = {
                'G': {kg0: 0, kg1: 1},
                'H': {kh0: 0, kh1: 1},
            }
            output = (cout.shuffle(), sum_.shuffle())
            yield compress(optimize(dumps(output)), 9)

    def __call__(self, gates: bytes):
        """
        Calculate the total sum iteratively.

        :param gates: A compressed and pickled gate pair.
        """
        cout, sum_ = loads(decompress(gates))
        self.__cout = self['G', cout]
        self.__sum |= self['H', sum_] << self.__offset
        self.__offset += 1

    def __getitem__(self, item: Tuple[str, int]) -> int:
        """
        Find the value that corresponds to the item.

        :param item: A tuple of gate id, label.
        :return: Either ``0`` or ``1``.
        """
        gate, label = item
        return self.__lookups[gate][label]

    def __int__(self) -> int:
        """Return the sum of the numbers as an integer."""
        return (self.__cout << self.__offset) | self.__sum

    def __str__(self) -> str:
        """Return the sum of the numbers as a bit string."""
        return format(int(self), 'b')
