from abc import ABC, abstractmethod
from itertools import product
from secrets import _sysrand  # noqa
from typing import Tuple, Optional

from .op import OP


class Gate(ABC):
    """Abstract circuit gate class."""
    @abstractmethod
    def __call__(self) -> Tuple[int, int]:
        """Return the label and point-and-permute bit of the gate."""
        pass

    @property
    @abstractmethod
    def output(self) -> Tuple[int, int, int]:
        """The output of the gate."""
        pass

    @staticmethod
    def keygen() -> int:
        """Generate a random 128-bit integer key."""
        return _sysrand.getrandbits(128)

    @staticmethod
    def bitgen() -> int:
        """Generate a random bit (``0`` or ``1``)."""
        return _sysrand.choice((0, 1))


class InputGate(Gate):
    """
    Circuit input gate class.

    :arg value: The input bit (must be ``0`` or ``1``).
    """
    def __init__(self, value: int):
        assert value in (0, 1)
        self.__value = value
        self.__output = ()

    def __call__(self) -> Tuple[int, int]:
        return self.output[:2][self.__value], self.output[-1] ^ self.__value

    @property
    def output(self) -> Tuple[int, int, int]:
        if not self.__output:
            self.__output = (self.keygen(), self.keygen(), self.bitgen())
        return self.__output


class LogicGate(Gate):
    """
    Circuit logic gate class.

    :arg op: The operation of the gate.
    :arg inputs: The inputs of the gate.
    """
    def __init__(self, op: OP, *inputs: Gate):
        assert len(op) == len(inputs)
        self.__op = op
        self.__inputs = inputs
        self.__output = ()
        self.__lookup = {}

    def __call__(self) -> Tuple[int, int]:
        keys, flags = zip(*(gate() for gate in self.__inputs))
        value = self.decrypt(self[flags], keys)
        return value >> 1, value & 1

    def __getitem__(self, item: Tuple[int, ...]) -> int:
        """
        Look for the item in the gate's lookup output.

        :param item: A tuple of labels.
        :return: An output label.
        """
        # ensure the output has been initialised
        return self.output and self.__lookup[item]

    @property
    def output(self) -> Tuple[int, int, int]:
        if not self.__output:
            input_vals = [gate.output for gate in self.__inputs]
            keys, flags = zip(*((v[:2], v[-1]) for v in input_vals))
            output_keys = (self.keygen(), self.keygen())
            xor_flag = self.bitgen()
            for key in product((0, 1), repeat=len(input_vals)):
                indices = [val ^ flag for val, flag in zip(key, flags)]
                enc_keys = [enc[idx] for enc, idx in zip(keys, indices)]
                gate_value = self.__op(*indices)
                out_value = output_keys[gate_value]
                xor_value = xor_flag ^ gate_value
                self.__lookup[key] = self.encrypt(
                    (out_value << 1) | xor_value,
                    tuple(reversed(enc_keys))
                )
            self.__output = output_keys + (xor_flag,)
        return self.__output

    def shuffle(self) -> 'LogicGate':
        """Return the gate after shuffling the output."""
        # keep the point-and-permute bit in place
        output = list(self.__output[:-1])
        _sysrand.shuffle(output)
        self.__output = tuple(output) + (self.__output[-1],)
        return self

    @classmethod
    def encrypt(cls, value: int, keys: Tuple[int, ...]) -> int:
        """
        Recursively encrypt the given value.

        :param value: The value to be encrypted.
        :param keys: The encryption keys.
        :return: The encrypted value.
        """
        if len(keys) == 0:
            return value
        return cls.encrypt(value ^ keys[0], keys[1:])

    @classmethod
    def decrypt(cls, value: int, keys: Tuple[int, ...]) -> int:
        """
        Recursively decrypt the given value.

        :param value: The value to be decrypted.
        :param keys: The decryption keys.
        :return: The decrypted value.
        """
        # delegate to encrypt since we're just using xor
        return cls.encrypt(value, keys)
