from gzip import compress, decompress
from pickle import dumps, loads
from pickletools import optimize


class Evaluator:
    """The person that evaluates the circuit."""
    def __call__(self, circuit: bytes) -> bytes:
        """
        Evaluate the circuit and returns the labels.

        :param circuit: A compressed and pickled sequence of gates.
        :return: A compressed and pickled sequence of output labels.
        """
        gates = loads(decompress(circuit))
        result = tuple(gate()[0] for gate in gates)
        return compress(optimize(dumps(result)), 9)
