from .evaluator import Evaluator
from .garbler import Garbler

try:
    # prompt the user for input
    a, b = map(int, input('Enter two integers: ').split())
except Exception as err:
    # exit on invalid input
    from sys import stderr
    print('ERROR:', err, file=stderr)
    exit(1)
else:
    # initialise A and B
    alice = Garbler(a, b)
    bob = Evaluator()

    # iterate through the circuits
    # and calculate the result
    for circuit in alice:
        alice(bob(circuit))

    # print the result
    print(a, '+', b, '=', int(alice))
