import enum
import operator


@enum.unique
class OP(enum.Enum):
    """Enum class representing the supported gate operations."""
    AND = '__and__'  #: A boolean ``and`` gate.
    OR = '__or__'  #: A boolean ``or`` gate.
    XOR = '__xor__'  #: A boolean ``xor`` gate.
    NOT = '__not__'  #: A unary ``not`` gate.

    def __str__(self) -> str:
        """Return the name of the gate."""
        return self.name

    def __len__(self) -> int:
        """
        Return the number of inputs the gate accepts.

        :return: ``1`` for ``NOT``, ``2`` for other gates.
        """
        return 2 if self.name != 'NOT' else 1

    def __call__(self, *inputs: int) -> int:
        """
        Run the given inputs through the gate.

        :param inputs: Any number of ``0`` or ``1`` bits.
        :return: The result of the gate operation.
        """
        return int(getattr(operator, self.value)(*inputs))
